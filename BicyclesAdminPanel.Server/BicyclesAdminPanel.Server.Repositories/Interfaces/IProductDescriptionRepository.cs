﻿using System.Threading.Tasks;
using BicyclesAdminPanel.Server.DataAccessLayer.Entities;

namespace BicyclesAdminPanel.Server.Repositories.Interfaces
{
    /// <summary>
    /// Product description repository's interface
    /// </summary>
    public interface IProductDescriptionRepository
    {
        /// <summary>
        /// Gets the bicycle description by culture.
        /// </summary>
        /// <param name="productModelId">The product model identifier.</param>
        /// <param name="cultureName">Name of the culture.</param>
        /// <returns></returns>
        Task<ProductDescription> GetProductDescriptionByCulture(int productModelId, string cultureName);
    }
}
