﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BicyclesAdminPanel.Server.DataAccessLayer.Entities;

namespace BicyclesAdminPanel.Server.Repositories.Interfaces
{
    /// <summary>
    /// Product cateogry repository's interface
    /// </summary>
    public interface IProductCategoryRepository
    {
        /// <summary>
        /// Gets all categories.
        /// </summary>
        /// <returns></returns>
        Task<List<ProductCategory>> GetAllProductCategories();
    }
}
