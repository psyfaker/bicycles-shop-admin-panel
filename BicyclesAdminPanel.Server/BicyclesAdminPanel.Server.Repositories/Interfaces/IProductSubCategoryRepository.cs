﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BicyclesAdminPanel.Server.DataAccessLayer.Entities;

namespace BicyclesAdminPanel.Server.Repositories.Interfaces
{
    /// <summary>
    /// Product subCategory repository interface
    /// </summary>
    public interface IProductSubCategoryRepository
    {
        /// <summary>
        /// Gets the name of the product sub category by.
        /// </summary>
        /// <param name="subCategoryName">Name of the sub category.</param>
        /// <returns></returns>
        Task<ProductSubcategory> GetProductSubCategoryByName(string subCategoryName);

        /// <summary>
        /// Gets all product sub categories.
        /// </summary>
        /// <returns></returns>
        Task<List<ProductSubcategory>> GetAllProductSubCategories();

        /// <summary>
        /// Gets the category sub categories.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns></returns>
        Task<List<ProductSubcategory>> GetCategorySubCategories(int categoryId);
    }
}
