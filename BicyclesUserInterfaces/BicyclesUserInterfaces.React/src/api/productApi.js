import {
  GetRequest,
  PostRequest,
  DeleteRequest,
  PutRequest
} from "./requestService";

export async function loadFiveTheMostPopularBicycles() {
  try {
    let response = await GetRequest("/api/Value/GetBicycles");
    return response.json();
  } catch (err) {
    throw err;
  }
}

export async function loadProductDetails(productId) {
  try {
    let queryParameters = new Map();
    queryParameters.set("productId", productId);

    let response = await GetRequest(
      "/api/Value/GetProductDetails",
      queryParameters
    );
    return response.json();
  } catch (err) {
    throw err;
  }
}

export async function findProductsByName(productName) {
  try {
    let queryParameters = new Map();
    queryParameters.set("productName", productName);
    let response = await GetRequest(
      "/api/Value/GetProductsByName",
      queryParameters
    );

    return response.json();
  } catch (err) {
    throw err;
  }
}

export async function deleteProduct(productId) {
  try {
    return await DeleteRequest("/api/Value/DeleteProduct", productId);
  } catch (err) {
    throw err;
  }
}

export async function updateProduct(product) {
  try {
    await PutRequest(product, "/api/Value/EditProduct");
    return product;
  } catch (err) {
    throw err;
  }
}

export async function addProduct(newProduct) {
  try {
    let response = await PostRequest(newProduct, "/api/Value/AddProduct");
    return response.json();
  } catch (err) {
    throw err;
  }
}

export async function loadProductSubcategories() {
  try {
    let response = await GetRequest(
      "/api/Categories/GetAllProductSubCategories"
    );
    return response.json();
  } catch (err) {
    throw err;
  }
}
