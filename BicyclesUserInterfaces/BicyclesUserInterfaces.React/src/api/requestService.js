import apiUrl from './url_config';

export function GetRequest(methodUrl, queryParameters) {
    if (queryParameters) {
        let request = BuildQueryParameters(queryParameters);
        return fetch(`${apiUrl.baseUrl}${methodUrl}${request}`);
    }

    return fetch(`${apiUrl.baseUrl}${methodUrl}`);
}

export function PutRequest(data, methodUrl) {
    let headers = { 'Content-Type': 'application/json' };
    return fetch(`${apiUrl.baseUrl}${methodUrl}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(data)
    });
}

export function PostRequest(data, methodUrl) {
    let headers = { 'Content-Type': 'application/json' };
    return fetch(`${apiUrl.baseUrl}${methodUrl}`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(data)
    });
}

export function DeleteRequest(methodUrl, objectId) { 
    let uri = `${apiUrl.baseUrl}${methodUrl}?id=${objectId}`;
    return fetch(`${apiUrl.baseUrl}${methodUrl}?id=${objectId}`,{
        method: 'DELETE',
    });
}


function BuildQueryParameters(queryParameters) {
    let uri = '?';
    
    queryParameters.forEach((value, key) =>{
        uri += `${key}=${value}&`; 
    });
    return uri.slice(0, -1);
}